"""
AUTHOR:  Frankie Liuzzi
EMAIL: slivuh@gmail.com
Intro to Linguistics Final Project

----------------------------------------------------------------
                       sentiment and genre modeling
----------------------------------------------------------------
"""


import collections
import nltk.classify.util, nltk.metrics
import itertools
from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import movie_reviews,brown
import re,string,sys

pattern = re.compile('([^\s\w]|_)+')

def bigram_word_feats(words, score_fn=BigramAssocMeasures.chi_sq, n=200):
    bigram_finder = BigramCollocationFinder.from_words(words)
    bigrams = bigram_finder.nbest(score_fn, n)
    return dict([(ngram, True) for ngram in itertools.chain(words, bigrams)])

def evaluate_sentiment_classifier(featx,filename):
    
    negfile = open("rt-polarity.neg")
    posfile = open("rt-polarity.pos")
 
    negfeats = [(featx(pattern.sub('', line).split()), 'neg') for line in negfile]
    posfeats = [(featx(pattern.sub('', line).split()), 'pos') for line in posfile]
    #print posfeats
    negcutoff = len(negfeats)*9/10
    poscutoff = len(posfeats)*9/10
 
    trainfeats = negfeats[:negcutoff] + posfeats[:poscutoff]
    testfeats = negfeats[negcutoff:] + posfeats[poscutoff:]
 
    classifier = NaiveBayesClassifier.train(trainfeats)
    refsets = collections.defaultdict(set)
    testsets = collections.defaultdict(set)
 
    for i, (feats, label) in enumerate(testfeats):
            refsets[label].add(i)
            observed = classifier.classify(feats)
            testsets[observed].add(i)
 
    print 'accuracy:', nltk.classify.util.accuracy(classifier, testfeats)
    #print 'pos precision:', nltk.metrics.precision(refsets['pos'], testsets['pos'])
    #print 'pos recall:', nltk.metrics.recall(refsets['pos'], testsets['pos'])
    #print 'neg precision:', nltk.metrics.precision(refsets['neg'], testsets['neg'])
    #print 'neg recall:', nltk.metrics.recall(refsets['neg'], testsets['neg'])
    classifier.show_most_informative_features()


    print "\n\n TESTS"

    if filename == None:    
        string = "He is a genuinely nice person."
        print "'" + string + "'     sentiment: " + classifier.classify(bigram_word_feats(string.split()))

        string = "You are a sad excuse for a human being."
        print "'" + string + "'     sentiment: " + classifier.classify(bigram_word_feats(string.split()))

        string = "You would not believe how amazing her voice is."
        print "'" + string + "'     sentiment: " + classifier.classify(bigram_word_feats(string.split()))

        string = "You would'nt believe how amazing her voice is."
        print "'" + string + "'     sentiment: " + classifier.classify(bigram_word_feats(string.split()))

        string = "I went to the store."
        print "'" + string + "'     sentiment: " + classifier.classify(bigram_word_feats(string.split()))

        string = "I do not hate you."
        print "'" + string + "'     sentiment: " + classifier.classify(bigram_word_feats(string.split()))

        string = "I hate it when you're so far away from me."
        print "'" + string + "'     sentiment: " + classifier.classify(bigram_word_feats(string.split()))

    else:
        string = open(filename, 'r').read()
        print "\n'" + string + "'\n\nSentiment: " + classifier.classify(bigram_word_feats(string.split()))

def evaluate_genre_classifier(featx,filename):
    fiction_fileids = brown.fileids('science_fiction')
    fiction_feats = [(featx(brown.words(fileids=[f])), 'science_fiction') for f in fiction_fileids]

    #print fiction_feats
    
    nonfiction_fileids = brown.fileids('news')
    nonfiction_feats = [(featx(brown.words(fileids=[f])), 'news') for f in nonfiction_fileids]

    romance_fileids = brown.fileids('romance')
    romance_feats = [(featx(brown.words(fileids=[f])), 'romance') for f in romance_fileids]

    cutoff = len(fiction_feats)*3/4

    #print len(fiction_feats)

    trainfeats = fiction_feats[:cutoff] + nonfiction_feats[:cutoff] + romance_feats[:cutoff]
    testfeats = romance_feats[cutoff:] + nonfiction_feats[cutoff:] + romance_feats[cutoff:]
    #print trainfeats

    classifier = NaiveBayesClassifier.train(trainfeats)
    #print fiction_feats

    #print 'Accuracy: %4.2f' % nltk.classify.accuracy(classifier, test_set) 

    classifier = NaiveBayesClassifier.train(trainfeats)
    refsets = collections.defaultdict(set)
    testsets = collections.defaultdict(set)
 
    for i, (feats, label) in enumerate(testfeats):
            refsets[label].add(i)
            observed = classifier.classify(feats)
            testsets[observed].add(i)
 
    print 'accuracy:', nltk.classify.util.accuracy(classifier, testfeats)
    classifier.show_most_informative_features()
    print "\n\n TESTS"

    if filename == None: 
        string = "We have initial reports that there has been an explosion at the Boston Marathon."
        print "'" + string + "'     sentiment: " + classifier.classify(bigram_word_feats(string.split()))

        string = "The commander ordered the lasers be fired."
        print "'" + string + "'     sentiment: " + classifier.classify(bigram_word_feats(string.split()))

        string = "He whispered into her ear at dinner."
        print "'" + string + "'     sentiment: " + classifier.classify(bigram_word_feats(string.split()))

    else:
        string = open(filename, 'r').read()
        print "\n'" + string + "'\n\nSentiment: " + classifier.classify(bigram_word_feats(string.split()))


if __name__ == "__main__":
    if len(sys.argv) == 2:
        if str(sys.argv[1]) == "-sent":
            print '-------------sentiment classifier----------------'
            evaluate_sentiment_classifier(bigram_word_feats,None)
        else:
            print '-------------genre classifier----------------'
            evaluate_genre_classifier(bigram_word_feats,None)
    elif len(sys.argv) == 3:
        if str(sys.argv[1]) == "-sent":
            print '-------------sentiment classifier----------------'
            evaluate_sentiment_classifier(bigram_word_feats,sys.argv[2])
        else:
            print '-------------genre classifier----------------'
            evaluate_genre_classifier(bigram_word_feats,sys.argv[2])
    else:
        print 'USAGE:  ling_liuzzi -<app>  <filepath> \nARGUMENTS:\n       app:   sent, genre\n       file:   filepath        <---- optional'
